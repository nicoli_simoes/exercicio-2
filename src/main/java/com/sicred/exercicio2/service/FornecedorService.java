package com.sicred.exercicio2.service;

import com.sicred.exercicio2.model.Fornecedor;
import com.sicred.exercicio2.repository.FornecedorRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FornecedorService {
    @Autowired
    private FornecedorRepository fornecedorRepository;
    
    public Fornecedor addFornecedor(Fornecedor fornecedor){
        return fornecedorRepository.save(fornecedor);
    }
    
    public List<Fornecedor> listAllFornecedors(){
        return fornecedorRepository.findAll();
    }
        
    public Optional<Fornecedor> listOneFornecedors(Long id){
        return fornecedorRepository.findById(id);
    }

    public Fornecedor alter(Long id, Fornecedor fornecedor){
       Fornecedor fornecedorAlter = valideteFornecedor(id);
        BeanUtils.copyProperties(fornecedor, fornecedorAlter, "id");
        return fornecedorRepository.save(fornecedorAlter);
    }

    public Fornecedor valideteFornecedor(Long id) {
        Optional<Fornecedor> fornecedor = fornecedorRepository.findById(id);  
        if(fornecedor.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return  fornecedor.get();
    }


}
