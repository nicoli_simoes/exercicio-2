package com.sicred.exercicio2.service;

import com.sicred.exercicio2.model.Produto;
import com.sicred.exercicio2.repository.ProdutoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {
    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto addProduto(Produto produto){
        return produtoRepository.save(produto);
    }

    public List<Produto> listAllProdutos(){
        return produtoRepository.findAll();
    }

    public Optional<Produto> listOneProdutos(Long id){
        return produtoRepository.findById(id);
    }

    public Produto alter(Long id, Produto produto){
       Produto produtoAlter = valideteProduto(id);
        BeanUtils.copyProperties(produto, produtoAlter, "id");
        return produtoRepository.save(produtoAlter);
    }

    public Produto valideteProduto(Long id) {
        Optional<Produto> produto = produtoRepository.findById(id);
        if(produto.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return  produto.get();
    }


}
