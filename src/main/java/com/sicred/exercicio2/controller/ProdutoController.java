package com.sicred.exercicio2.controller;

import com.sicred.exercicio2.model.Produto;
import com.sicred.exercicio2.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {
    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public List<Produto> listAllProdutos() {
        return produtoService.listAllProdutos();
    }

    @PostMapping
    public ResponseEntity<Produto> addProduto(@RequestBody Produto produto){
        Produto produtoAdd = produtoService.addProduto(produto);
        return ResponseEntity.status(HttpStatus.CREATED).body(produtoAdd);
    }
}
